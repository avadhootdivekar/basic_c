#include<stdio.h>

int main()
{
 int i,j,n,flag=0,t,a[10];
 
 printf("Enter no of elements : ");                           // INPUT
 scanf("%d",&n);                                              // FROM
 
 for(i=0;i<n;i++)                                             // USER
 {
  printf("Enter No %d :",i+1);
  scanf("%d",&a[i]);
 }

 for(i=1;i<n;i++)                                             // ACTUAL SORTING CODE
 {
  for(j=0;j<i;j++)
  {
   if(a[j]>a[i])
   {
    t=a[j];
    a[j]=a[i];
    flag=1;
    break;
   }
  }

  if(flag==1)
  {
   flag=j;
   for(j=i;j>flag;j--)
   {
    a[j]=a[j-1];
   }
   a[flag+1]=t;
   flag=0;
  }
  
 }

 printf("Sorted array is : ");                                 // PRINTING OUTPUT
 for(i=0;i<n;i++)
 printf("\nElement %d :  %d",i+1,a[i]);

 printf("\n");
return 0;
}
