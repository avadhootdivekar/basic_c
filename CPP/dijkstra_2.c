#include<stdio.h>

 struct node{
int node_id,dist,done;
struct nq* nbr;
struct node* next;
};

struct nq{
int dist;
struct node* nbr_node;
struct nq* next;
};

typedef struct node *nodeptr;
typedef struct nq *nqptr;

 nodeptr head;

void get_nodes();
nodeptr new_node();
void add_nbr(int,int,int);
void traverse();
nodeptr find_next();
nodeptr find_by_id(int);
void print();


void main()
{
 head=(nodeptr)malloc(sizeof(struct node));
 get_nodes();
 traverse();
 print();
 printf("\n");

}

 nodeptr new_node()
{
 nodeptr p;
 p=(nodeptr)malloc(sizeof(struct node));
 p->nbr=NULL;
return p;
}

 void add_nbr(int id1,int id2,int dist)
{
 nodeptr nodea,nodeb;
 nqptr p,q;
 p=(struct nq*)malloc(sizeof(struct nq));

 nodea=find_by_id(id1);
 nodeb=find_by_id(id2);

 if(nodea==NULL || nodeb==NULL)
  {
    printf("\nPlease Check Nodes");
    goto END;
  }
 
 if(nodea->nbr==NULL)
  {
    printf("\nCASE 1");
    nodea->nbr=p;
   p->nbr_node=nodeb;
   p->dist=dist;
   p->next=NULL;
  }
 else
 {
    printf("\nCASE 2");
   q=nodea->nbr;
   while(q->next!=NULL)
      q=q->next;
   q->next=p;
   p->nbr_node=nodeb;
   p->dist=dist;
   p->next=NULL;
 }
  goto HERE;

END :
 printf("\nADD NBR ENDS HERE.");
HERE:
 printf("\n");
}

 nodeptr find_by_id(int id)
{
 int i;
 nodeptr p;
 p=head;

 while(p!=NULL)
 {
   if(p->node_id==id)
      return p;
  p=p->next;
 }
  return NULL;
}

 void get_nodes()
{
 int i,n,id1,id2,dist;
 nodeptr p,q;
 p=head;
 printf("\nEnter no of nodes : ");
 scanf("%d",&n);
 printf("\nEnter nodes strating from start point : ");

 for(i=0;i<n-1;i++)
 {
  printf("\nEnter node-ID :");
  scanf("%d",&(p->node_id));
  p->nbr=NULL;
  p->dist=-1;
  p->done=0;
  p->next=new_node();
  p=p->next;
 }
  printf("\nEnter node-ID :");
  scanf("%d",&(p->node_id));
  p->nbr=NULL;
  p->dist=-1;
  p->done=0;
  p->next=NULL;

 printf("\nEnter -1 to terminate entries.");
 while(1)
 {
   printf("\nEnter From 'ID' To 'ID' and 'Distance'");
   scanf("%d ",&id1);
   if(id1<0)goto HERE;
   scanf("%d %d" ,&id2,&dist);
   p=find_by_id(id1);
   q=find_by_id(id2);
   if(p==NULL || q==NULL)
    {printf("\nNode not Found!");goto HERE;}

   add_nbr(id1,id2,dist);
 }
HERE:
 printf("");

}

 nodeptr find_next()
{
 int n,id;
 n=9999;
 id=-1;
 nodeptr p;
 p=head;
 while(p!=NULL)
 {
  if(p->done==0 && (p->dist)<n && p->dist!=-1)
   {
     id=p->node_id;
   }
   p=p->next;
 }
 
 if(id==-1)
   return NULL;

 p=find_by_id(id);
 return p;
}

 void traverse()
{
 nodeptr p,nbr_n;
 nqptr q;

 p=head;
 q=NULL;
 head->dist=0;
 while(p!=NULL)
 {
  q=p->nbr;
  while(q!=NULL)
  {
    nbr_n=(q->nbr_node);
   
   if((nbr_n->dist)>((q->dist)+(p->dist))||(nbr_n->dist==-1))
    {
     (nbr_n->dist)=(q->dist)+(p->dist);
     }
   q=q->next;
  }
  p->done=1;
  p=find_next();
 }
}

 void print()
{
 nodeptr p;
 p=head;
 while(p!=NULL)
 {
  printf("\n  %d",p->dist);
  p=p->next;
 }
}
