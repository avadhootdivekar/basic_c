#include<stdio.h>


struct fh
{
	int bftype;
	long int bfsize;
	int reserve1;
	int reserve2;
	long int bfoffset;
};

struct infh
{
	long int bisize;
	long int biwidth;
	long int biheight;
	int biplanes;
	int bitcount;
	long int bitcomp;
	long int bisizeimg;
	long int bihorrresol;
	long int bivertresol;
	long int bicrl;
	long int binoimpcrl;
};

void main()
{
  struct fh fh1;
  struct infh infh1;
  FILE *fp;
  char A[50];
  printf("\nEnter PATH : ");
  scanf("%s",&A);
  fp=fopen(A,"rb+");
  fread(&fh1,sizeof(fh1),1,fp);
  fread(&infh1,sizeof(infh1),1,fp);
  printf("%d",fh1.bfsize);
  printf("\n%d",infh1.bitcount);
  printf("\n%d",infh1.biwidth);
  printf("\n%d",infh1.biheight);
  printf("\n");
};
