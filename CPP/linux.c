#include<stdio.h>
#include<unistd.h>
#include<sys/types.h>

void main()
{
 int pid;
 printf("Process ID : %d\n",getpid());

 pid=fork();

 if(pid==0)
   {
    printf("This is child process.\n");
     execl ( "/bin/ls","-al", "/etc", NULL ) ;
     printf ( "Child: After exec( )\n") ;

   }
 else
    printf("This is parent process.\n");

}
