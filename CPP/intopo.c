#include<stdio.h>

struct stack
{
 char op[50];
 int top;
};

typedef struct stack stack;
//struct stack *s;

void convert(char infix[50],char postfix[50]);
int op(char);
void push(char,stack *s);
char pop(stack *s);
int prec(char);
int not_empty(stack *s);

void main()
{
 char infix[50],postfix[50];
 printf("\nEnter Expression : ");
 gets(infix);
 
 printf("\nPASS 0");
 printf("\n");

 convert(infix,postfix);

 printf("\nPASS 0");
 printf("\n");

 printf("\nPostfix Form is : ");
 puts(postfix);
}


 void convert(char infix[50],char postfix[50])
{
  int i,j;
  char x,y;
  stack *s;

  i=0;
  j=0;
  (s->top)=-1;

 printf("\nPASS 0");
 printf("\n");

  while((infix[i])!='\0')
  {
   x=infix[i];

 printf("\nPASS 0");
 printf("\n");

   if(x=='(')
     push(x,s);
   else if(x==')')
   {
    while((y=pop(s))!='(')
     postfix[j++]=y;
   }
   else if(op(x))
   {
    if((prec(x)<prec(s->op[s->top])) && not_empty(s))
     postfix[j++]=pop(s);
    
 printf("\nPASS OP");
 printf("\n");

     push(x,s);
   }
   else
    postfix[j++]=x;

  i++;
  }

 printf("\nPASS 0");
 printf("\n");

 while(not_empty(s))
   postfix[j++]=pop(s);

  postfix[j]='\0';
}


 int op(char x)
{
  if(x=='+' || x=='-' || x=='*' || x=='/' || x=='^' )
   return 1;
  else 
   return 0;
}

 int prec(char x)
{
  if(x=='^')
   return 3;
  else if(x=='*' || x=='/')
   return 2;
  else if(x=='+' || x=='-')
   return 1;
  else
    return 0;
}

void push(char x,stack *s)
{
 s->op[(s->top)]=x;
 s->top=s->top+1;
}

char pop(stack *s)
{
 return s->op[(s->top)--];
}

int not_empty(stack *s)
{
  if(s->top==-1)
    return 1;
  else 
    return 0;
}
