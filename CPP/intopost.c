#include<stdio.h>

struct stack{
char op[50];
int top;
};

char emp(struct stack*);
char pop(struct stack*);
void push(struct stack* ,char ch);
int symb(char);
int prec(char ,char);

void main()
{
 int i,j,e;
 char s[100],post[100];
 struct stack *p;
 p->top=-1;
 printf("Enter the Equation : ");
 scanf("%s",&s);

 i=0;
 j=0;
 while(s[i]!='\0')
 {
  if(symb(s[i]))
  {
    post[j]=s[i];
    j++;
  }
  else
  {
   while(((emp(p))!="y") && prec(p->op[p->top],s[i]))
   {
    post[j]=pop(p);
    j++;
   }
   push(p,s[i]);
  }

 }

 while(((emp(p))!="y"))
 {
  post[j]=pop(p);
  j++;
 }

 printf("\nPostfix Expression is :\n%s",post);

}


char emp(struct stack *p)
{
 int i;
 if((p->top)==-1)
  return "y";
 else 
  return "n";
}

char pop(struct stack *p)
{
 return (p->op[(p->top)--]);
}

void push(struct stack *p,char ch)
{
 (p->op[++(p->top)])=ch;
}

int prec(char a,char b)
{
 if(a=="*"||a=="/")
   return 1;
 else if((a=="+"||a=="-")&&(b=="+"||b=="-"))
   return 1;
 else 
   return 0;
}

int symb(char x)
{
 if(x!="+" && x!="-" && x!="*" &&x!="/" )
  return 1;
 else 
  return 0;
}
