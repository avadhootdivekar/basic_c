#include<stdio.h>
#include<math.h>


void main()
{
 int i,m;
 float y,x,d,num,den;

 printf("\nEnter No of which root is to be found:");
 scanf("%f",&y);
 printf("\nEnter order of root: ");
 scanf("%d",&m);

 x=y/(m*m*m);

 printf("\n");
 for(i=0;i<=(3*m);i++)
 {
  num=powf(x,(float)m);
  num=y-num;
  den=powf(x,(float)(m-1));
  den=m*den;

  d=num/den;
  x=x+d;

  printf("\t %f",x);
 };

 printf("\n%dth root of %.0f is : %f",m,y,x);

 printf("\n");
 
}
