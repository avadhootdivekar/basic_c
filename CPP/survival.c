#include<stdio.h>
#include<math.h>

void main()
{
 int cases,r=1;			//r=>round
 long int n,start,k,p,el;
 char s[100];
 FILE *fp,*fq;

 fp=fopen("input","r");
 fq=fopen("output","w");
 if(fp==NULL)
{
	printf("\nERROR WHILE OPENING I/P FILE.");
	return 1;
};

 if(fq==NULL)
{
	printf("\nERROR WHILE OPENING O/P FILE.");
	return 1;
};

 fgets(s,99,fp);
 cases=atoi(s);

 while(fgets(s,99,fp)!=NULL)
{
	r=1;
	el=2;
	n=atoi(s);
	start=1;

	if(n%2==0)
	{
		n=n-1;
		el=3;
		start=1;
	}
	else
	{
		el=5;
		start=3;
	}
	r=2;
	 while(n>start)
	{
		p=(long int)powf(2.0,(float)r);
		k=(long int)el%p;
		if((n%p)==k)
		{
			n=n-p/2;
			el=el+p/2;
			if(n<=start)
			{
				sprintf(s,"%ld",start);
				fputs(s,fq);
				fputs("\n",fq);
			}
		}
		else
		{
			el=start+2*p;
			start=start+p;
			if(n<=start)
			{
				sprintf(s,"%ld",n);
				fputs(s,fq);
				fputs("\n",fq);
			};
		};
		r++;
	};
}
fclose(fq);
fclose(fp);
};
