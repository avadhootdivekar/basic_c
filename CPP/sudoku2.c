#include<stdio.h>

  FILE *fp;
  int a[9][9];
  int b[9][9];

void read_file();
void write_file(int);
void init();
void print_sudoku();
int invalid();


void main()
{
 int i,j,n;

 init();

 printf("\nEnter No of elements : ");
 scanf("%d",&n);
 write_file(n);
 read_file();
   print_sudoku();
 while(invalid())
  {
   printf("\nEnter element : ");
   scanf("%d %d ",&i,&j);
   scanf("%d",&a[i-1][j-1]);
   print_sudoku();
  }

 printf("\nCONGRATS!");
}

void init()
{
 int i,j;
 for(i=0;i<9;i++)
   for(j=0;j<9;j++)
    { a[i][j]=0; printf("%d",a[i][j]);}
}

void write_file(int n)
{
 int i,j,k,m;
 fp=fopen("entry.dat","w");

 for(k=0;k<n;k++)
 {
  scanf("%d %d",&i,&j);
  scanf("%d",&a[i-1][j-1]);
 }
 
 for(i=0;i<9;i++)
 {
   for(j=0;j<9;j++)
    {
      if(a[i][j]>0)
       fprintf(fp,"%d%d%d",i+1,j+1,a[i][j]);
    }
 }
 fclose(fp);
}


void read_file()
{
 int i,j,n,t;
 char s[20];
 fp=fopen("entry.dat","r");

 fscanf(fp,"%s",s);
 n=atoi(s);
 
 while(n>0)
 {
  t=(n%10);n/=10;
  j=(n%10);n/=10;
  i=(n%10);n/=10;
  a[i-1][j-1]=t;
  printf("%d %d %d \n",i,j,a[i-1][j-1]);
 }

 fclose(fp);
}

 void print_sudoku()
{
 int i,j;
 for(i=0;i<9;i++)
 {
  if(i==3 || i==6)
    printf("\n");
  printf("\n   \t");
  for(j=0;j<9;j++)
   {
     if(j==3 || j==6)
      printf("  \t  ");
     printf("    %d",a[i][j]);
   }
 }

 printf("\n\n");
}

int invalid()
{
 int i,j,k,x,y,d,sqr,flag;
 x=0;   y=0;  sqr=0;

 for(i=0;i<9;i++)
 {
  y=0;
   for(j=0;j<9;j++)
  {
    y+=a[i][j];
  }
  if(y!=45)
   {flag=0; goto BREAK;}
 }
 for(j=0;j<9;j++)
 {
  y=0;
   for(i=0;i<9;i++)
  {
    y+=a[i][j];
  }
  if(y!=45)
   {flag=0; goto BREAK;}
 }
 y=0;
/* for(i=0;i<9;i++)
   for(j=0;j<9;j++)
     y+=a[i][j];
 if(y!=45)
  goto BREAK;
*/

 for(k=0;k<3;k++)
 {
  sqr=0;
   for(i=0;i<3;i++)
      for(j=3*k;j<3*(k+1);j++)
         sqr+=a[i][j];
  if(sqr!=45)
   goto BREAK;

  sqr=0;
   for(i=3;i<6;i++)
      for(j=3*k;j<3*(k+1);j++)
         sqr+=a[i][j];
  if(sqr!=45)
   goto BREAK;
 
  sqr=-0;
   for(i=6;i<9;i++)
      for(j=3*k;j<3*(k+1);j++)
         sqr+=a[i][j];
  if(sqr!=45)
   goto BREAK;
 
 }

 return 0;

BREAK :
 return 1;
}
