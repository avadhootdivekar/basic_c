#include<stdio.h>

int main()
{
 int i,j,t,n,a[10];

 printf("Enter no of elements : ");
 scanf("%d",&n);
 for(i=0;i<n;i++)
 { 
  printf("Enter no %d : ",i+1);
  scanf("%d",&a[i]);
 }

 for(i=n-1;i>0;i--)
 {
  for(j=0;j<i;j++)
  {
   if(a[j]>a[j+1])
   {
    a[j]=a[j]+a[j+1];
    a[j+1]=a[j]-a[j+1];
    a[j]=a[j]-a[j+1];
   }
  }
 }
 
 printf("\nSorted array is : ");
 for(i=0;i<n;i++)
 printf("\nElement %d  :  %d",i+1,a[i]);
 printf("\n");

return 0;
}
