#include<stdio.h>
#include <stdbool.h>
typedef enum { FALSE, TRUE } boolean;

struct node{
int w;
int i;
struct node *next;
struct node *back;

};
typedef struct node *nodeptr;

struct list{
	int i;
	struct list *next;
};
typedef struct list *listptr;

struct queue{

	struct list *base;
	struct queue *next;
};
typedef struct queue *qptr;

nodeptr new();
void delete(nodeptr);
void add(nodeptr);
listptr new_l();
void add_index(nodeptr);
void init_q();
void new_list();

nodeptr base;
qptr que_base;
qptr que;
listptr l;


void main()
{
   int i,j,N,W,t,test=0;
   nodeptr p,q,temp;
 //  qptr que_base,qq;
   listptr l;
   init_q();

   scanf("%d %d",&N,&W);

	base=new();
	scanf("%d",&(base->w));
	base->i=0;
	for(i=1;i<N;i++)
	{
		p=new();
		scanf("%d",&(p->w));
		p->i=i;
		add(p);
	};
   q=base;

	while(base!=NULL)
	{

		new_list();
		t=0;
		q=base;
		t=t+(q->w);
		test=q->i;

		(que->base)->i=test;
		l=que->base;

		delete(q);
		if(base==NULL)
			break;

		q=base;		
		while((t<W)&&(q!=NULL))
		{
			t=t+(q->w);
			
			if(t>W)
			{
				t=t-(q->w);
				q=q->next;
				continue;
			}
			else if(t==W)
			{
				add_index(q);
				p=q;
				q=q->next;
				delete(p);
				break;
			}
			else
			{
				add_index(q);
				p=q;
				q=q->next;
				delete(p);
			};
		};
	};

	que=que_base;
	que=que->next;
	i=0;
		while(que!=NULL)
		{
			i=i+1;
			que=que->next;
		}

	printf("%d",i);
	que=que_base;
	que=que->next;
			l=que->base;
			
		while(que!=NULL)
		{
			printf("\n");
			i=0;
			l=que->base;
			while(l!=NULL)
			{
				l=l->next;
				i++;
			}

			printf("%d ",i);
			l=que->base;
			while(l!=NULL)
			{
				printf(" %d",l->i);
				l=l->next;
			}
			que=que->next;
			if(que!=NULL)
			l=que->base;
		}

}

nodeptr new()
{
  nodeptr p;
  p=(nodeptr)malloc(sizeof(struct node));
  p->next=NULL;
  p->back=NULL;
  return p;
}
 
void delete(nodeptr p)
{
  if(p->next==NULL && p->back==NULL)
  {
	if(p==base)
		base=NULL;

    free(p);
  }
  else if(p->next==NULL)
  {
	(p->back)->next=NULL;
	free(p);
  }
  else if(p->back==NULL)
  {

	if(p==base)
		base=p->next;

	(p->next)->back=NULL;
	free(p);
  }
  else
  {
	(p->next)->back=p->back;
	(p->back)->next=p->next;
  }
};

void add(nodeptr p)
{
   nodeptr q;
   q=base;
	if((p->w)>=(q->w))
	{
		base=p;
		p->next=q;
		q->back=p;
	}
	else
	{
		q=q->next;
		while(q!=NULL)
		{
			if((p->w)>=(q->w))
			{
				(q->back)->next=p;
				(p->back)=(q->back);
				(p->next)=q;
				(q->back)=p;
				break;
			}
			else
				q=q->next;
		}
			if(q==NULL)
			{
				q=base;
				while(q->next!=NULL)
					q=q->next;
				q->next=p;
				p->back=q;
			}
	}
};


void init_q()
{
	que_base=(qptr)malloc(sizeof(struct queue));
	(que_base)->next=NULL;
	(que_base)->base=NULL;
	que=que_base;
};

void new_list()
{
  (que->next)=(qptr)malloc(sizeof(struct queue));
   que=que->next;
  l=(listptr)malloc(sizeof(struct list));
  l->next=NULL;
  l->i=0;
  que->base=l;
  que->next=NULL;
};

void add_index(nodeptr p)
{
	l->next=(listptr)malloc(sizeof(struct list));
	l=l->next;
	l->i=p->i;
	l->next=NULL;
};

