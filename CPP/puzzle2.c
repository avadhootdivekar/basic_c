#include<stdio.h>

struct stack{
 int i; 
 int j;
 struct stack *next;
};

typedef struct stack* stkptr;

 int n,m;
 int a[20][20];
 stkptr top;

int valid(int,int);
stkptr pop();
void push(int,int);
int empty();
void stk_init();
void print();
void optimize();
int stksize();

void main()
{
 int i,j,x;
 stkptr p;
 
 stk_init();
 x=0;
 m=1;

 printf("\nEnter size : ");
 scanf("%d",&n);
 printf("\t%d",n);

 for(i=0;i<n;i++)
  for(j=0;j<n;j++)
    scanf("%d",&a[i][j]);

 i=0;
 j=0;

 while(a[n-1][n-1]==0 && x<50)
 {
   if(valid(i+1,j+1))
     {push(i++,j++);printf("\nPASS %d",x++);}
   else if(valid(i+1,j))
     {push(i++,j);printf("\nPASS %d",x++);}
   else if(valid(i+1,j-1))
     {push(i++,j--);printf("\nPASS %d",x++);}
   else if(valid(i,j-1))
     {push(i,j--);printf("\nPASS %d",x++);}
   else if(valid(i-1,j-1))
     {push(i--,j--);printf("\nPASS %d",x++);}
   else if(valid(i-1,j))
     {push(i--,j);printf("\nPASS %d",x++);}
   else if(valid(i-1,j+1))
     {push(i--,j++);printf("\nPASS %d",x++);}
   else if(valid(i,j+1))
     {push(i,j++);printf("\nPASS %d",x++);}
   else
     { 
       if(empty())
        { printf("\nNo path ! "); break;}
       else
        {
         a[i][j]=-1;
         p=pop();
         i=p->i;
         j=p->j;
        }
     }
  if(i==n-1 && j==n-1)
    a[i][j]=m;
 }

  if(a[n-1][n-1]==1)
    printf("\nPath found! \n");
  print();

 optimize();
 printf("\n");
}

void stk_init()
{ top=NULL;}

void push(int i,int j)
{
 stkptr p;
 p=(stkptr)malloc(sizeof(struct stack));
 if(top==NULL)
  (p->next)=NULL;
 else
  (p->next)=top;
 top=p;
 top->i=i;
 top->j=j;
 a[i][j]=m++;
}

stkptr pop()
{
 stkptr p;
 p=(stkptr)malloc(sizeof(struct stack));
 p=top;
 if(top->next !=NULL)
   top=top->next;
 else  
    top=NULL;
 //a[p->i][p->j]=-1;
 return p;
}

int empty()
{
 if(top==NULL)
  return 1;
 return 0;
} 

int valid(int i,int j)
{
  if(i>-1 && j>-1 && i<n && j<n && a[i][j]==0)
     return 1;
  return 0;
}

void print()
{
  int i,j;
  for(i=0;i<n;i++)
  {
    printf("\n");
    for(j=0;j<n;j++)
      printf("\t%d",a[i][j]);
  }
}


int stksize()
{
 stkptr p;
 p=top;
 n=0;
 while(p!=NULL)
  {p=p->next;n++;}
 return n;
}

void optimize()
{
  int n;
  stkptr p;
  n=stkptr();
  p=pop();
  p=pop();



}
